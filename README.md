# My Card
## Tech-stack
* [Jetpack](https://developer.android.com/jetpack)
    * [Navigation](https://developer.android.com/guide/navigation) - The Navigation Architecture Component simplifies implementing navigation, while also helping you visualize your app's navigation flow.
    * [SafeArgs](https://developer.android.com/guide/navigation/navigation-pass-data#Safe-args) - plugin generates simple object and builder classes for type-safe navigation and access to any associated arguments.
    * [Room](https://developer.android.com/topic/libraries/architecture/room) - The Room persistence library provides an abstraction layer over SQLite to allow for more robust database access while harnessing the full power of SQLite.
    * [Android KTX](https://developer.android.com/kotlin/ktx) - Android KTX is a set of Kotlin extensions that are included with Android Jetpack.
    * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - store and manage UI-related data in a lifecycle conscious way
    * [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle) - perform action when lifecycle state changes
    * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - notify views about database change
    * [Data Binding](https://developer.android.com/topic/libraries/data-binding) - library that allows you to bind UI components in your layouts to data sources in your app using a declarative format rather than programmatically.
* [Coil](https://github.com/coil-kt/coil) - image loading library with Kotlin idiomatic API
* [RxJava](https://github.com/ReactiveX/RxJava) - a library for composing asynchronous and event-based programs using observable sequences for the Java VM.
* [Gson](https://github.com/google/gson) - A Java serialization/deserialization library to convert Java Objects into JSON and back
* [Transition Animation](https://developer.android.com/training/transitions) - Android's transition framework allows you to animate all kinds of motion in your UI by simply providing the starting layout and the ending layout.
* [QRUtils](https://github.com/chtgupta/QRUtils-Android) - QRUtils is a set of pre-built, customizable UI components for creating and reading QR codes.


![image](screen3.png)![image](screen2.png)![image](screen1.png)