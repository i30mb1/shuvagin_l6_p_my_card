package com.example.shuvagin_l6_t1

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController

class MainActivity : AppCompatActivity() {

    //    val viewModel: CardViewModel by viewModels()
    lateinit var viewModel: CardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar_activity_main))
        supportActionBar?.setDisplayShowTitleEnabled(false)

        viewModel = ViewModelProvider(
            findNavController(R.id.nav_host_fragment).getViewModelStoreOwner(R.id.nav_main)
        ).get(CardViewModel::class.java)
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

    }

}
