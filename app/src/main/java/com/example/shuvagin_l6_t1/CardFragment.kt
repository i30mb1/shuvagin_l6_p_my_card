package com.example.shuvagin_l6_t1

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.telephony.PhoneNumberUtils
import android.util.Patterns
import android.util.TimingLogger
import android.view.*
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import coil.api.load
import coil.transform.CircleCropTransformation
import com.example.shuvagin_l6_t1.CustomDialog.Companion.DIALOG_TYPE_CALL_ACTION
import com.example.shuvagin_l6_t1.CustomDialog.Companion.DIALOG_TYPE_EMAIL_ACTION
import com.example.shuvagin_l6_t1.CustomDialog.Companion.DIALOG_TYPE_NORMAL
import com.example.shuvagin_l6_t1.databinding.BottomDrawerBinding
import com.example.shuvagin_l6_t1.databinding.FragmentCardBinding
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.concurrent.TimeUnit

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

class CardFragment : Fragment() {

    private lateinit var menu: Menu
    private lateinit var binding: FragmentCardBinding
    lateinit var db: CardDatabase
    private val viewModel by navGraphViewModels<CardViewModel>(R.id.nav_main)
    val editable: ObservableBoolean = ObservableBoolean(false)
    val IsFormValidWithAnimation = { view: TextInputEditText, pattern: Boolean, errorString: Int, viewAnimate: TextInputLayout? ->
        Boolean
        subject.onNext("")
        val valid: Boolean
        if (!view.text.isNullOrEmpty() && pattern) {
            view.error = null
            valid = true
        } else {
            view.error = getString(errorString)
            valid = false
            viewAnimate?.let {
                val translate = TranslateAnimation(-10f, 10f, 0f, 0f)
                translate.duration = (50L..80L step 5).shuffled().first()
                translate.repeatMode = Animation.REVERSE
                translate.repeatCount = 3
                translate.interpolator = LinearInterpolator()
                viewAnimate.startAnimation(translate)
                view.requestFocus()
            }

        }
        valid
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_card, container, false)
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            (activity as MainActivity).supportActionBar?.hide()
        }
        db = CardDatabase(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.card = CardInfo()
        binding.fragment = this
        viewModel.getCallTrigger().observe(viewLifecycleOwner, Observer { it?.let { getCall() } })
        viewModel.getEmailTrigger().observe(viewLifecycleOwner, Observer { it?.let { getEmail() } })

        setHasOptionsMenu(true)
        retainInstance = true

        setChangedListeners()
        setOnBackPressedAction()
    }

    val dispatcher by lazy { requireActivity().onBackPressedDispatcher }
    lateinit var callback: OnBackPressedCallback
    private fun setOnBackPressedAction() {
        callback = dispatcher.addCallback(this) {
            if (editable.get()) {
                menu.findItem(R.id.menu_save).isVisible = false
                menu.findItem(R.id.menu_edit).isVisible = true
                editable.set(false)
                setNullForEditTextError()
                clearFormFocusAndHideKeyboard()
                context?.hideKeyboard(binding.inputFragmentCardEmail)
                CardFragmentAnimations.initTransitionAnimation(binding, editable)
                //                callback.isEnabled = false
            } else {
                requireActivity().finish()
            }
        }
    }

    fun tryToCreateFile() {
        if (binding.etFragmentCardFullName.text.isNullOrEmpty()) {
            val navDirections = CustomDialogDirections.actionGlobalCustomDialog(
                getString(R.string.card_fragment_empty_filed), DIALOG_TYPE_NORMAL, null
            )
            findNavController().navigate(navDirections)
        } else {
            Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "text/plain"
                putExtra(Intent.EXTRA_TITLE, "${binding.card?.fullName ?: "empty"}.txt")
                if (this.resolveActivity(requireContext().packageManager) != null) {
                    startActivityForResult(this, WRITE_STORAGE_CODE)
                } else {
                    saveInFileBackupMethod()
                }
            }
        }
    }

    private fun writeInCreatedFile(uri: Uri) {
        requireActivity().contentResolver.openFileDescriptor(uri, "w")?.use {
            FileOutputStream(it.fileDescriptor).use { fileOutputStream ->
                fileOutputStream.write(binding.card?.serializeGson()?.toByteArray() ?: "empty".toByteArray())
            }
        }
        showSnackBarOpenPath(null)
    }

    private fun saveInFileBackupMethod() {
        File(context?.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "${binding.card?.fullName ?: "empty"}.txt").printWriter().use {
            it.write(binding.card?.serializeGson() ?: "empty DataBinding")
        }
        showSnackBarOpenPath(context?.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)?.path?.toUri())
    }

    private fun showSnackBarOpenPath(uri: Uri?) {
        val snack = Snackbar.make(binding.root, getString(R.string.card_fragment_saved), Snackbar.LENGTH_LONG)
        when (uri) {
            null -> {
                snack.setAction(getString(R.string.card_fragment_open)) {
                    Intent(Intent.ACTION_VIEW).apply {
                        setDataAndType(uri, "application/*")
                        if (this.resolveActivity(requireActivity().packageManager) != null) startActivity(this)
                    }
                }
            }
            else -> {
                snack.setAction(getString(R.string.card_fragment_open)) {
                    loadFromFile()
                }
            }
        }
        snack.show()
    }

    fun loadFromFile() {
        Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "*/*"
            if (this.resolveActivity(requireActivity().packageManager) != null) startActivityForResult(this, WRITE_STORAGE_CODE)
        }
    }

    private fun setNullForEditTextError() {
        binding.etFragmentCardFullName.error = null
        binding.etFragmentCardFullPhone.error = null
        binding.etFragmentCardFullEmail.error = null
    }

    fun emailClickListener() {
        if (binding.etFragmentCardFullEmail.text.isNullOrBlank()) {
            val navDirections = CustomDialogDirections.actionGlobalCustomDialog(
                getString(R.string.fragment_card_email_must_be_not_empty), DIALOG_TYPE_NORMAL, null
            )
            findNavController().navigate(navDirections)
        } else {
            showEmailDialog()
        }
    }

    private fun showEmailDialog() {
        val navDirections = CustomDialogDirections.actionGlobalCustomDialog(
            getString(R.string.card_fragment_desire_to_email), DIALOG_TYPE_EMAIL_ACTION, null
        )
        findNavController().navigate(navDirections)
    }

    private fun getEmail() {
        Intent(Intent.ACTION_SENDTO).apply {
            data = "mailto:${binding.etFragmentCardFullEmail.text.toString()}".toUri()
            startActivity(Intent.createChooser(this, getString(R.string.fragment_card_email_via)))
        }
    }

    fun callClickListener() {
        if (binding.etFragmentCardFullPhone.text.isNullOrBlank()) {
            val navDirections = CustomDialogDirections.actionGlobalCustomDialog(
                getString(R.string.fragment_card_phone_must_be_not_empty), DIALOG_TYPE_NORMAL, null
            )
            findNavController().navigate(navDirections)
        } else {
            showCallDialog()
        }
    }

    private fun showCallDialog() {
        val navDirections = CustomDialogDirections.actionGlobalCustomDialog(
            getString(R.string.card_fragment_desire_to_call), DIALOG_TYPE_CALL_ACTION, null
        )
        findNavController().navigate(navDirections)
    }

    private fun getCall() = getCall(binding.etFragmentCardFullPhone.text.toString())

    private fun getCall(number: String) {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), CALL_PERMISSION_CODE)
        } else {
            Intent(Intent.ACTION_DIAL).apply {
                data = ("tel:$number").toUri()
                if (this.resolveActivity(requireContext().packageManager) != null) startActivity(this)
            }
        }
    }

    val disposable = CompositeDisposable()
    private val subject = PublishSubject.create<String>()
    private fun setChangedListeners() {
        val debounceTimeout = 1000L
        val subscribe = subject.debounce(debounceTimeout, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { generateQR() }
        disposable.add(subscribe)
        binding.etFragmentCardFullName.apply {
            doOnTextChanged { _, _, _, _ ->
                IsFormValidWithAnimation(
                    this, true, R.string.card_fragment_empty_filed, null
                )
            }
        }
        binding.etFragmentCardFullEmail.apply {
            doOnTextChanged { text, _, _, _ ->
                IsFormValidWithAnimation(
                    this, Patterns.EMAIL_ADDRESS.matcher(text.toString()).matches(), R.string.card_fragment_field_emal_invalid, null
                )
            }
        }
        binding.etFragmentCardFullPhone.apply {
            doOnTextChanged { text, _, _, _ ->
                IsFormValidWithAnimation(
                    this, Patterns.PHONE.matcher(text.toString()).matches(), R.string.card_fragment_field_phone_invalid, null
                )
            }
        }
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
        this.menu = menu
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        toggleMenu()
        return super.onOptionsItemSelected(item)
    }

    private fun generateQR() = binding.ivFragmentCardQr.setData(binding.card?.serializeGson()).build()

    private fun toggleMenu() {
        when (editable.get()) {
            false -> {
                menu.findItem(R.id.menu_save).isVisible = true
                menu.findItem(R.id.menu_edit).isVisible = false
                editable.set(true)
                CardFragmentAnimations.initTransitionAnimation(binding, editable)
            }
            true -> {
                val isEditTextNameValid = IsFormValidWithAnimation(
                    binding.etFragmentCardFullName, true, R.string.card_fragment_empty_filed, binding.inputFragmentCardFullName
                )
                val isEditTextPhoneValid = IsFormValidWithAnimation(
                    binding.etFragmentCardFullPhone, PhoneNumberUtils.isGlobalPhoneNumber(binding.etFragmentCardFullPhone.text.toString()),
                    R.string.card_fragment_field_phone_invalid,
                    binding.inputFragmentCardPhone
                )
                val isEditTextEmailValid = IsFormValidWithAnimation(
                    binding.etFragmentCardFullEmail, Patterns.EMAIL_ADDRESS.matcher(binding.etFragmentCardFullEmail.text.toString()).matches(),
                    R.string.card_fragment_field_emal_invalid,
                    binding.inputFragmentCardEmail
                )
                if (isEditTextNameValid && isEditTextEmailValid && isEditTextPhoneValid) {
                    menu.findItem(R.id.menu_edit).isVisible = true
                    menu.findItem(R.id.menu_save).isVisible = false
                    editable.set(false)
                    CardFragmentAnimations.initTransitionAnimation(binding, editable)
                    saveCurrentCardInDb()
                } else {
                    val navDirections = CustomDialogDirections.actionGlobalCustomDialog(
                        getString(R.string.card_fragment_empty_filed), DIALOG_TYPE_NORMAL, null
                    )
                    findNavController().navigate(navDirections)
                }
            }
        }
        clearFormFocusAndHideKeyboard()
    }

    private fun clearFormFocusAndHideKeyboard() {
        activity?.window?.decorView?.clearFocus()
        context?.hideKeyboard(binding.inputFragmentCardEmail)
    }

    private fun saveCurrentCardInDb() {
        if (binding.etFragmentCardFullName.text.isNullOrEmpty()) return

        binding.card?.let {
            db.cardDao().insert(it).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
        }
    }

    var bottomSheetDialog: BottomSheetDialog? = null

    fun showBottomSheetDialog() {
        bottomSheetDialog = BottomSheetDialog(requireContext())
        val binding = DataBindingUtil.inflate<BottomDrawerBinding>(layoutInflater, R.layout.bottom_drawer, null, false)
        val adapter = RecyclerViewCard(this)
        binding.rvBottomDrawer.adapter = adapter
        bottomSheetDialog?.setContentView(binding.root)
        bottomSheetDialog?.show()
        val allCardFromDb = db.cardDao().getAllRx().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { list: MutableList<CardInfo> -> adapter.setList(list) }
        disposable.add(allCardFromDb)
    }

    fun setCard(user: CardInfo) {
        binding.card = user
        user.image.let {
            val bitmap = BitmapFactory.decodeByteArray(it, 0, it.size)
            binding.ivFragmentCardPhoto.load(bitmap) { transformations(CircleCropTransformation()) }
            setNullForEditTextError()
        }
    }

    fun getPhoto() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
        } else {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, CAMERA_REQUEST)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CAMERA_PERMISSION_CODE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) getPhoto()
            CALL_PERMISSION_CODE -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) getCall()
            else -> {
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) return
        when (requestCode) {
            CAMERA_REQUEST -> {
                val bitmap = data?.extras?.get("data") as Bitmap
                binding.card?.image = convertImageToArrayByte(bitmap)
                binding.ivFragmentCardPhoto.load(bitmap) { transformations(CircleCropTransformation()) }
            }
            LOAD_STORAGE_CODE -> {
                data?.data?.let {
                    val fileDescriptor = activity?.contentResolver?.openFileDescriptor(it, "r")
                    if (fileDescriptor != null) {
                        val fis = FileInputStream(fileDescriptor.fileDescriptor)
                        fis.bufferedReader().use { bufferedReader ->
                            val cardInfo = CardInfo().deserializeGson(bufferedReader.readText())
                            val bitmap = BitmapFactory.decodeByteArray(cardInfo.image, 0, cardInfo.image.size)
                            binding.card = cardInfo
                            binding.ivFragmentCardPhoto.load(bitmap) { transformations(CircleCropTransformation()) }
                        }

                    }
                }
            }
            WRITE_STORAGE_CODE -> {
                data?.data?.also { writeInCreatedFile(it) }
            }
        }

    }

    private fun convertImageToArrayByte(bitmap: Bitmap): ByteArray {
        val timingLogger = TimingLogger("N7", "convertImageToArrayByte")
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream)
        timingLogger.dumpToLog()
        return stream.toByteArray()
    }

    companion object {
        const val CAMERA_PERMISSION_CODE = 12
        const val CALL_PERMISSION_CODE = 13
        const val CAMERA_REQUEST = 18
        const val LOAD_STORAGE_CODE = 17
        const val WRITE_STORAGE_CODE = 19
    }
}
