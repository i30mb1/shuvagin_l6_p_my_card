package com.example.shuvagin_l6_t1

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.PrecomputedTextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import coil.transform.CircleCropTransformation
import com.example.shuvagin_l6_t1.databinding.ItemCardInfoBinding

class RecyclerViewCard(val fragment: CardFragment) : RecyclerView.Adapter<RecyclerViewCard.ViewHolder>() {

    private var list: MutableList<CardInfo> = mutableListOf()

    fun setList(newList: MutableList<CardInfo>) {
        val diffItemCallback = object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = list[oldItemPosition] === newList[newItemPosition]

            override fun getOldListSize(): Int = list.size

            override fun getNewListSize(): Int = newList.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = (list[oldItemPosition] == newList[newItemPosition])

        }
        list = newList
        DiffUtil.calculateDiff(diffItemCallback).dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemCardInfoBinding: ItemCardInfoBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_card_info, parent, false)
        return ViewHolder(itemCardInfoBinding)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.set(list[position])

    inner class ViewHolder(private val itemCardInfoBinding: ItemCardInfoBinding) : RecyclerView.ViewHolder(itemCardInfoBinding.root) {

        fun set(card: CardInfo) {
            itemCardInfoBinding.card = card
            itemCardInfoBinding.executePendingBindings()
            val bitmap = BitmapFactory.decodeByteArray(card.image, 0, card.image.size)
                itemCardInfoBinding.ivItemCardInfo.load(bitmap) { transformations(CircleCropTransformation()) }
            // Precomputed Text for better performances
            itemCardInfoBinding.tvItemCardInfoEmail.setTextFuture(
                PrecomputedTextCompat.getTextFuture(card.email, itemCardInfoBinding.tvItemCardInfoEmail.textMetricsParamsCompat, null)
            )
            itemCardInfoBinding.cvItemCardInfo.setOnClickListener {
                fragment.setCard(itemCardInfoBinding.card!!)
                fragment.bottomSheetDialog?.dismiss()
            }
        }
    }

}