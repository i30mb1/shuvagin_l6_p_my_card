package com.example.shuvagin_l6_t1

import android.annotation.SuppressLint
import android.content.Context
import android.view.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.ObservableBoolean
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce
import androidx.transition.*
import com.example.shuvagin_l6_t1.databinding.FragmentCardBinding

class CardFragmentAnimations {

    companion object {

        fun initTransitionAnimation(binding: FragmentCardBinding, editable: ObservableBoolean) {

            val transitionSet = TransitionSet()
            transitionSet.addTransition(Slide(Gravity.START).apply {
                addTarget(binding.bFragmentCardCall)

            })
            transitionSet.addTransition(Slide(Gravity.END).apply {
                addTarget(binding.bFragmentCardEmail)
            })
            transitionSet.addTransition(ChangeBounds().apply {
                addTarget(binding.ivFragmentCardQr)

            })
            transitionSet.addTransition(Fade().apply {
                addTarget(binding.ivFragmentCardPhotoEdit)
            })
            TransitionManager.beginDelayedTransition(binding.root as ViewGroup, transitionSet)
            //            val paramsIvQr = binding.ivFragmentCardQr.layoutParams as ConstraintLayout.LayoutParams
            //            //        val paramsFl = binding.flFragmentCardConteiner.layoutParams as ConstraintLayout.LayoutParams
            //            if (editable.get()) {
            //                paramsIvQr.topToBottom = ConstraintLayout.LayoutParams.UNSET
            //                paramsIvQr.topToTop = binding.flFragmentCardConteiner.id
            //                paramsIvQr.bottomToBottom = binding.flFragmentCardConteiner.id
            //                paramsIvQr.endToEnd = binding.root.id
            //                paramsIvQr.startToStart = ConstraintLayout.LayoutParams.UNSET
            //            } else {
            //                paramsIvQr.topToBottom = binding.bFragmentCardCall.id
            //                paramsIvQr.topToTop = binding.bFragmentCardCall.id
            //                paramsIvQr.bottomToBottom = binding.root.id
            //                paramsIvQr.endToEnd = binding.root.id
            //                paramsIvQr.startToStart = binding.root.id
            //            }
        }

        lateinit var xAnimation: SpringAnimation
        lateinit var yAnimation: SpringAnimation

        lateinit var scaleXAnimation: SpringAnimation
        lateinit var scaleYAnimation: SpringAnimation
        lateinit var scaleGestureDetector: ScaleGestureDetector

        val STIFFNESS = SpringForce.STIFFNESS_MEDIUM
        val DAMPING_RATIO = SpringForce.DAMPING_RATIO_HIGH_BOUNCY

        @SuppressLint("ClickableViewAccessibility")
        fun initSpringAnimationDrag(binding: FragmentCardBinding, dragView: View) {
            dragView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    xAnimation = createSpringAnimation(dragView, SpringAnimation.X, dragView.x, STIFFNESS, DAMPING_RATIO)
                    yAnimation = createSpringAnimation(dragView, SpringAnimation.Y, dragView.y, STIFFNESS, DAMPING_RATIO)
                    dragView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })

            var dX = 0f
            var dY = 0f
            dragView.setOnTouchListener { view, event ->
                when (event.actionMasked) {
                    MotionEvent.ACTION_DOWN -> {
                        // capture the difference between view's top left corner and touch point
                        dX = view.x - event.rawX
                        dY = view.y - event.rawY

                        // cancel animations so we can grab the view during previous animation
                        xAnimation.cancel()
                        yAnimation.cancel()
                    }
                    MotionEvent.ACTION_MOVE -> {
                        //  a different approach would be to change the view's LayoutParams.
                        binding.ivFragmentCardQr.animate()
                            .x(event.rawX + dX)
                            .y(event.rawY + dY)
                            .setDuration(0)
                            .start()
                    }
                    MotionEvent.ACTION_UP -> {
                        xAnimation.start()
                        yAnimation.start()
                    }
                }
                true
            }
        }

        fun initSpringAnimationScaling(scalingView: View, context: Context) {
            val INITIAL_SCALE = 1f
            val STIFFNESS = SpringForce.STIFFNESS_MEDIUM
            val DAMPING_RATIO = SpringForce.DAMPING_RATIO_HIGH_BOUNCY
            scaleXAnimation = createSpringAnimation(scalingView, SpringAnimation.SCALE_X, INITIAL_SCALE, STIFFNESS, DAMPING_RATIO)
            scaleYAnimation = createSpringAnimation(scalingView, SpringAnimation.SCALE_Y, INITIAL_SCALE, STIFFNESS, DAMPING_RATIO)
            setupPinchToZoom(scalingView, context)
            scalingView.setOnTouchListener { _, event ->
                if (event.action == MotionEvent.ACTION_UP) {
                    scaleXAnimation.start()
                    scaleYAnimation.start()
                } else {
                    // cancel animations so we can grab the view during previous animation
                    scaleXAnimation.cancel()
                    scaleYAnimation.cancel()

                    // pass touch event to ScaleGestureDetector
                    scaleGestureDetector.onTouchEvent(event)
                }
                true
            }
        }

        private fun setupPinchToZoom(scalingView: View, context: Context) {
            var scaleFactor = 1f
            scaleGestureDetector = ScaleGestureDetector(context, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
                override fun onScale(detector: ScaleGestureDetector): Boolean {
                    scaleFactor *= detector.scaleFactor
                    scalingView.scaleX *= scaleFactor
                    scalingView.scaleY *= scaleFactor
                    return true
                }
            })
        }

        private fun createSpringAnimation(view: View, property: DynamicAnimation.ViewProperty, finalPosition: Float, stiffness: Float, dampingRatio: Float): SpringAnimation {
            val animation = SpringAnimation(view, property)
            val spring = SpringForce(finalPosition)
            spring.stiffness = stiffness
            spring.dampingRatio = dampingRatio
            animation.spring = spring
            return animation
        }

    }

}
