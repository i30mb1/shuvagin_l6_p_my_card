package com.example.shuvagin_l6_t1

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.io.Serializable

class CustomDialog : DialogFragment() {

    private val args: CustomDialogArgs by navArgs()
    private val viewModel by navGraphViewModels<CardViewModel>(R.id.nav_main)

    companion object {
        val DIALOG_TYPE_NORMAL = 1
        val DIALOG_TYPE_CALL_ACTION = 2
        val DIALOG_TYPE_EMAIL_ACTION = 3
    }

    var listener: Call? = null

    interface Call : Serializable {
        fun buttonPressed()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.setWindowAnimations(R.style.DialogAnimations)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        listener = args.listener
        val dialogBuilder = MaterialAlertDialogBuilder(context).setMessage(args.message)
        when (args.dialogType) {
            DIALOG_TYPE_NORMAL -> dialogBuilder.setPositiveButton(android.R.string.ok, null)
            DIALOG_TYPE_CALL_ACTION -> {
                dialogBuilder.setPositiveButton(android.R.string.ok) { _, _ -> viewModel.triggerCall();listener?.buttonPressed() }
                dialogBuilder.setNegativeButton(android.R.string.no, null)
            }
            DIALOG_TYPE_EMAIL_ACTION -> {
                dialogBuilder.setPositiveButton(android.R.string.ok) { _, _ -> viewModel.triggerEmail() }
                dialogBuilder.setNegativeButton(android.R.string.no, null)
            }
        }
        return dialogBuilder.create()
    }
}