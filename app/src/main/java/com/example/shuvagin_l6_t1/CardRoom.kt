package com.example.shuvagin_l6_t1

import android.content.Context
import androidx.room.*
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
interface CardDao {
    @Query("SELECT * FROM CardEntity")
    fun getAll(): MutableList<CardInfo>

     @Query("SELECT * FROM CardEntity")
    fun getAllRx(): Single<MutableList<CardInfo>>

    @Insert
    fun insertAll(vararg cards: CardInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(card:CardInfo): Single<Long>

    @Delete
    fun delete(card: CardInfo)

    @Delete
    fun deleteRx(card: CardInfo):Single<Int>

    @Query("DELETE FROM CardEntity WHERE full_name =:name")
    fun deleteRxByName(name: String): Single<Int>

    @Update
    fun update(vararg cards: CardInfo)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateRx(vararg cards: CardInfo): Single<Int>

    @Query("SELECT * FROM CardEntity WHERE full_name=:name")
    fun loadCardByName(name: String): Single<CardInfo>
}

@Database(entities = arrayOf(CardInfo::class), version = 1)
abstract class CardDatabase : RoomDatabase() {
    abstract fun cardDao(): CardDao

    companion object {
        @Volatile
        private var instance: CardDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room
            .databaseBuilder(context, CardDatabase::class.java, "cards.db")
            .build()
    }
}