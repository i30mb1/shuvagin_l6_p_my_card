package com.example.shuvagin_l6_t1

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson

@Entity(tableName = "CardEntity")
 data class CardInfo(
    @PrimaryKey
    @ColumnInfo(name = "full_name",defaultValue = "")
    var fullName:String ="",
    @ColumnInfo(name = "phone",defaultValue = "")
    var phone:String = "",
    @ColumnInfo(name = "email",defaultValue = "")
    var email:String = "",
    @ColumnInfo(name = "image",defaultValue = "",typeAffinity = ColumnInfo.BLOB) var image: ByteArray = ByteArray(0)
)

fun CardInfo.serializeGson(): String = Gson().toJson(this)


fun CardInfo.deserializeGson(jsonString:String):CardInfo {
    return Gson().fromJson(jsonString, CardInfo::class.java)
}