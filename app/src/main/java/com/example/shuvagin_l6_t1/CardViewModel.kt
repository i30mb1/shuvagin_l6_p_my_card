package com.example.shuvagin_l6_t1

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CardViewModel : ViewModel() {

    val callTrigger: MutableLiveData<Boolean?> = MutableLiveData(null)
    val emailTrigger: MutableLiveData<Boolean?> = MutableLiveData(null)
    val backPressTrigger: MutableLiveData<Boolean?> = MutableLiveData(null)
    lateinit var db: CardDatabase

    init {

    }

    fun getCallTrigger(): LiveData<Boolean?> = callTrigger

    fun triggerCall() {
        callTrigger.value = true
        callTrigger.value = null
    }

    fun getEmailTrigger(): LiveData<Boolean?> = emailTrigger

    fun triggerEmail() {
        emailTrigger.value = true
        emailTrigger.value = null
    }

}
